import 'dart:math';

import 'package:crud_simple/core/models/books.dart';
import 'package:crud_simple/helper/database_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BookController extends GetxController {
  List<BooksModel> bookModel = [];

  bool isEdit = false;

  TextEditingController bookTitleController = TextEditingController();
  TextEditingController authorNameController = TextEditingController();
  TextEditingController pubDateController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    getAll();
  }

  showForm(int? id) {
    return Get.bottomSheet(
      Container(
        padding: EdgeInsets.only(
          // top: 25,
          bottom: 10,
          right: 15,
          left: 15,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Divider(),
            Container(
              margin: const EdgeInsets.only(top: 15.0, right: 20.0, left: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Book Title",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 14.0),
                  ),
                  Container(
                    transform: Matrix4.translationValues(0.0, -8.0, 0.0),
                    child: TextFormField(
                      controller: bookTitleController,
                      keyboardType: TextInputType.text,
                      textAlign: TextAlign.left,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                      ),
                      obscureText: false,
                      decoration: InputDecoration(
                          disabledBorder: UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          focusedBorder: new UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          hintText: "......",
                          hintStyle: TextStyle(color: Colors.grey)),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 0.0, right: 20.0, left: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Author",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 14.0),
                  ),
                  Container(
                    transform: Matrix4.translationValues(0.0, -8.0, 0.0),
                    child: TextFormField(
                      controller: authorNameController,
                      keyboardType: TextInputType.text,
                      textAlign: TextAlign.left,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                      ),
                      obscureText: false,
                      decoration: InputDecoration(
                          disabledBorder: UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          focusedBorder: new UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          hintText: "......",
                          hintStyle: TextStyle(color: Colors.grey)),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 0.0, right: 20.0, left: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Pub. Date",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 15.0),
                  ),
                  Container(
                    transform: Matrix4.translationValues(0.0, -8.0, 0.0),
                    child: TextFormField(
                      controller: pubDateController,
                      keyboardType: TextInputType.text,
                      textAlign: TextAlign.left,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                      ),
                      obscureText: false,
                      decoration: InputDecoration(
                          disabledBorder: UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          focusedBorder: new UnderlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Colors.transparent)),
                          hintText: "Ex : 12 Nov 2021",
                          hintStyle: TextStyle(color: Colors.grey)),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: ButtonTheme(
                minWidth: double.infinity,
                child: RaisedButton(
                  elevation: 0,
                  highlightElevation: 0,
                  padding: const EdgeInsets.symmetric(vertical: 14.0),
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                      fontSize: 15,
                    ),
                  ),
                  onPressed: () {
                    if (isEdit) {
                      validationEdit(id!);
                    } else {
                      validationAdd();
                    }
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(15),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      isDismissible: true,
      backgroundColor: Colors.transparent,
    );
  }

  getById(int id) async {
    await DatabaseHelper.instance.getById(id).then((value) {
      print(value[0]);
      print(isEdit);
      bookTitleController.text = value[0]['book_title'];
      authorNameController.text = value[0]['author_name'];
      pubDateController.text = value[0]['publication_date'];
      showForm(id);
    });
  }

  validationAdd() {
    if (bookTitleController.text.isEmpty ||
        authorNameController.text.isEmpty ||
        pubDateController.text.isEmpty) {
      Get.snackbar(
        "Oops!",
        "Fill all form",
        colorText: Colors.white,
        backgroundColor: Colors.red,
      );
    } else {
      addData();
    }
  }

  validationEdit(int id) {
    if (bookTitleController.text.isEmpty ||
        authorNameController.text.isEmpty ||
        pubDateController.text.isEmpty) {
      Get.snackbar(
        "Oops!",
        "Fill all form",
        colorText: Colors.white,
        backgroundColor: Colors.red,
      );
    } else {
      updateData(id);
    }
  }

  getAll() {
    bookModel.clear();
    update();
    DatabaseHelper.instance.queryAllRows().then((value) {
      value.forEach((element) {
        bookModel.add(
          BooksModel(
            id: element['id'],
            bookTitle: element['book_title'],
            authorName: element['author_name'],
            publicationDate: element['publication_date'].toString(),
          ),
        );
        update();
      });
    });
  }

  addData() async {
    await DatabaseHelper.instance.insert(
      BooksModel(
        id: Random().nextInt(9999999),
        bookTitle: bookTitleController.text.toString().trim(),
        authorName: authorNameController.text.toString().trim(),
        publicationDate: pubDateController.text.toString().trim(),
      ),
    );
    Get.back();
    bookTitleController.clear();
    authorNameController.clear();
    pubDateController.clear();
    getAll();
  }

  updateData(int id) async {
    await DatabaseHelper.instance.updateData(
      id,
      bookTitle: bookTitleController.text.toString().trim(),
      authorName: authorNameController.text.toString().trim(),
      pubDate: pubDateController.text.toString().trim(),
    );
    Get.back();
    bookTitleController.clear();
    authorNameController.clear();
    pubDateController.clear();
    getAll();
  }

  void delete(int id) async {
    await DatabaseHelper.instance.delete(id);
    bookModel.removeWhere((element) => element.id == id);
    update();
  }
}
