import 'package:crud_simple/core/controller/book_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(BookController());
    return GetBuilder(
      init: controller,
      builder: (_) {
        return Scaffold(
          floatingActionButton: Container(
            margin: EdgeInsets.only(
              bottom: 15,
              right: 15,
            ),
            child: FloatingActionButton(
              onPressed: () {
                controller.isEdit = false;
                controller.bookTitleController.clear();
                controller.authorNameController.clear();
                controller.pubDateController.clear();
                controller.showForm(null);
              },
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          ),
          appBar: AppBar(
            title: Text("Ahmad's Books Store"),
          ),
          body: ListView.builder(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.1),
            itemCount: controller.bookModel.length,
            itemBuilder: (ctx, index) {
              return ListTile(
                onLongPress: () {
                  controller.delete(controller.bookModel[index].id);
                },
                onTap: () {
                  controller.isEdit = true;
                  controller.getById(controller.bookModel[index].id);
                },
                title: Text(
                  controller.bookModel[index].bookTitle,
                ),
                subtitle: Text(
                  controller.bookModel[index].authorName,
                ),
                trailing: Text(
                  controller.bookModel[index].publicationDate,
                ),
              );
            },
          ),
        );
      },
    );
  }
}
