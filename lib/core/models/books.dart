class BooksModel {
  int id;
  String bookTitle;
  String authorName;
  String publicationDate;

  BooksModel({
    required this.id,
    required this.bookTitle,
    required this.authorName,
    required this.publicationDate,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['book_title'] = this.bookTitle;
    data['author_name'] = this.authorName;
    data['publication_date'] = this.publicationDate;
    return data;
  }
}
