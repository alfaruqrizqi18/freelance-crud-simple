import 'package:crud_simple/core/models/books.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static final _databaseName = "crud_simple.db";
  static final _databaseVersion = 1;

  static final table = "books";

  static final columnId = 'id';
  static final columnBookTitle = 'book_title';
  static final columnAuthorName = 'author_name';
  static final columnPublicationDate = 'publication_date';

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
            $columnId INTEGER PRIMARY KEY AUTOINCREMENT,
            $columnBookTitle STRING NOT NULL,
            $columnAuthorName STRING NOT NULL,
            $columnPublicationDate STRING NOT NULL
          )
          ''');
  }

  Future<int> insert(BooksModel booksModel) async {
    Database db = await instance.database;
    var res = await db.insert(table, booksModel.toJson());
    return res;
  }

  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await instance.database;
    var res = await db.query(table, orderBy: "$columnId DESC");
    return res;
  }

  Future<int> updateData(
    int id, {
    required String bookTitle,
    required String authorName,
    required String pubDate,
  }) async {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['book_title'] = bookTitle;
    data['author_name'] = authorName;
    data['publication_date'] = pubDate;
    Database db = await instance.database;
    var res = await db.update(
      table,
      data,
      where: '$columnId = ?',
      whereArgs: [id],
    );
    return res;
  }

  Future<List<Map<String, dynamic>>> getById(int id) async {
    Database db = await instance.database;
    return await db.query(
      table,
      where: '$columnId = ?',
      whereArgs: [id],
    );
  }

  Future<int> delete(int id) async {
    Database db = await instance.database;
    return await db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }

  Future clearTable() async {
    Database db = await instance.database;
    return await db.rawQuery("DELETE FROM $table");
  }
}
