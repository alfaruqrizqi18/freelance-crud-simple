import 'package:crud_simple/core/views/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

main() {
  setupRunApp();
}

setupRunApp() {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(
      GetMaterialApp(
        defaultTransition: Transition.fadeIn,
        builder: (BuildContext context, Widget? child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: child!,
          );
        },
        title: "CRUD Simple",
        theme: ThemeData.light(),
        debugShowCheckedModeBanner: false,
        home: HomePage(),
      ),
    );
  });
}
